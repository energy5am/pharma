#!/usr/bin/python3

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: pwgen

short_description: generate password hashes

version_added: "2.9"

description:
    - >
      This module generates password hashes.

options:
    hash_mode:
        description:
            - Record type
        required: false
        default: sha512
    password:
        description:
            - The password to hash. If not given, it is generated.
        required: false


author:
    - Fiona Hoyer <fiona.hoyer@splendid-internet.de>
'''

EXAMPLES = '''
# generate SHA512 password
- name: Generate SHA512 password
  pwgen:

# generate MySQL password
- name: Generate MySQL password
  pwgen:
    hash_mode: mysql

# hash a password
- name: Hash password with SHA512
  pwgen:
    password: 'PASSWORD'
'''

RETURN = '''
password:
    description: The password that was passed in or is generated
    type: str
    returned: always
hash:
    description: The password hash that this module generates
    type: str
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule

from hashlib import sha1
import crypt
import secrets
import string

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        hash_mode=dict(required=False, default='sha512',
                  choices=['sha512', 'mysql']),
        password=dict(type='str', required=False, default=None)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    if module.params['password'] is None:
        alphabet = string.ascii_letters + string.digits
        password = ''.join(secrets.choice(alphabet) for i in range(20))
    else:
        password = module.params['password']

    if module.params['hash_mode'] == 'mysql':
        hash = "*" + sha1(sha1(str(password).encode('utf-8')).digest()).hexdigest().upper()
    else:
        hash = crypt.crypt(str(password), crypt.mksalt(crypt.METHOD_SHA512))

    result['changed'] = True
    result['password'] = password
    result['hash'] = hash

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
